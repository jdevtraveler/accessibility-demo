package ar.com.bancar.uala.accessibilitydemo

import android.text.SpannableString
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ar.com.bancar.uala.accessibilitydemo.databinding.ItemBinding
import java.text.SimpleDateFormat
import java.util.*

class TransactionsAdapter(private val items: List<TransactionItem>) :
    RecyclerView.Adapter<TransactionsAdapter.ViewHolder>() {

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }


    inner class ViewHolder(private val binding: ItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: TransactionItem) {
            binding.run {
                title.text = item.title
                subtitle.text = item.subtitle

                setDate(item.date)
                if (item.amount.isNotEmpty())
                    setAmount(item.amount)
            }
        }

        private fun setDate(dateInMillis: String) {
            val calendar = Calendar.getInstance().apply {
                timeInMillis = dateInMillis.toLong()
            }
            val dateText = SimpleDateFormat(DATE_FORMAT, Locale("es", "AR")).format(calendar.time)

            val spannableString = SpannableString(dateText).apply {
                setSpan(
                    calendar.toDateTtsSpan(),
                    0,
                    dateText.length,
                    SpannableString.SPAN_EXCLUSIVE_EXCLUSIVE
                )
            }
            binding.date.text = spannableString
        }

        private fun setAmount(amountText: String) {
            val text = "$$amountText"
            val spannable = SpannableString(text).apply {
                setSpan(
                    amountText.toAmountTtsSpan(),
                    0,
                    text.length,
                    SpannableString.SPAN_EXCLUSIVE_EXCLUSIVE
                )
            }
            binding.amount.text = spannable

        }

    }

    companion object {
        const val DATE_FORMAT = "dd/MM/yy"
    }

}

