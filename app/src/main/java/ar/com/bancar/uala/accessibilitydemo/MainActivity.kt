package ar.com.bancar.uala.accessibilitydemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.SpannableString
import android.view.LayoutInflater
import ar.com.bancar.uala.accessibilitydemo.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    val balances = listOf("2343,40","456,69","300,00","-13,50")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupViews()
        setBalance("2345,39")
        setListener()
    }

    private fun setListener() {
        binding.run {
            addAmount.setOnClickListener {
                val newBalance = balances.random()
                setBalance(newBalance)

                val description = "Saldo actualizado: $newBalance"
                val startInd = description.indexOf(newBalance)
                val spannable = SpannableString(description).apply {
                    setSpan(
                        newBalance.toAmountTtsSpan(),
                        startInd,
                        startInd+newBalance.length,
                        SpannableString.SPAN_EXCLUSIVE_EXCLUSIVE
                    )
                }
                it.announceForAccessibility(spannable)
            }
        }
    }

    private fun setBalance(balance:String){
        val description = "Saldo: $balance"
        val startInd = description.indexOf(balance)
        val spannable = SpannableString(description).apply {
            setSpan(
                balance.toAmountTtsSpan(),
                startInd,
                startInd+balance.length,
                SpannableString.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
        binding.amount.run{
            text = "$$balance"
            contentDescription = spannable
        }
    }

    private fun setupViews() {
        val transactions = listOf(
            TransactionItem(
                "Depósito",
                "Depósito exitoso",
                "300,00",
                "1631204051000"
            ),
            TransactionItem("Depósito", "Depósito fallido", "", "1606407251000"),
            TransactionItem("Depósito", "Depósito exitoso", "350,30", "1584202451000")
        )
        binding.recycler.adapter = TransactionsAdapter(transactions)
    }
}