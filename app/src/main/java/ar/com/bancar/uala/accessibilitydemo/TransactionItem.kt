package ar.com.bancar.uala.accessibilitydemo

data class TransactionItem(val title: String, val subtitle : String, val amount: String, val date: String)