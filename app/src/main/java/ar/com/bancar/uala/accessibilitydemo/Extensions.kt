package ar.com.bancar.uala.accessibilitydemo

import android.text.style.TtsSpan
import java.util.*

//TtsSpan is a span for TextToSpeech spannable configuration
fun Calendar.toDateTtsSpan(): TtsSpan {
    return TtsSpan.DateBuilder().setDay(this.get(Calendar.DAY_OF_MONTH))
        .setMonth(this.get(Calendar.MONTH))
        .setYear(this.get(Calendar.YEAR))
        .build()
}

fun String.toAmountTtsSpan(): TtsSpan {
    val integerPart = this.split(',')[0]
    val fractionalPart = this.split(',')[1]
    return TtsSpan.MoneyBuilder()
//                .setQuantity("1000")
        .setIntegerPart(integerPart)
        .setFractionalPart(fractionalPart)
        .setCurrency("ARS")
        .build()
}